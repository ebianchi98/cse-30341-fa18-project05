#!/usr/bin/env python3
import subprocess
import re

addrlens = [8, 10, 12, 14, 16]
page_sizes = [8, 16, 32, 64]
apps = ["prime", "scan", "fibonacci","collatz"] 

print("======= CACHE SIZE TEST =======")

total_hits = 0
total_misses = 0

for a in apps:
	print("Application: " + a)
	for l in addrlens:
		for p in page_sizes:
			if a == "collatz":
				seq = subprocess.Popen(["seq","1","100"],stdout=subprocess.PIPE)
				s = subprocess.Popen(["./bin/func_{}".format(a), str(l), str(p), "3", "1"], stdin=seq.stdout, stdout=subprocess.PIPE)
			else:		
				s = subprocess.Popen(["./bin/func_{}".format(a), str(l), str(p), "3", "1"], stdout=subprocess.PIPE)
			out = str(s.communicate()[0])
			out = out.split('\\n')
			s.stdout.close()
			hits = 0
			misses = 0

			for entry in out:
				if re.match(r"Hits", entry) or re.match(r"b'Hits",entry):
					hits = float(entry.split()[2])
				if re.match(r"Misses", entry):
					misses = float(entry.split()[2])

			print("Address Length = {}, Page Size = {}".format(l,p))
			print("Hits: ",hits)
			print("Misses: ",misses)

	print()

print("======= END CACHE SIZE TEST =======")
print()

policies = [0, 1, 2, 3]

ttal_misses = 0
total_hits = 0

print("======= EVICTION POLICY TEST =======")

polsdict = {0 : "FIFO", 1 : "RANDOM", 2 : "LRU", 3 : "CLOCK"}

for pol in policies:
	total_misses = 0
	total_hits = 0	
	for a in apps:
		if a == "collatz":
			seq = subprocess.Popen(["seq","1","100"],stdout=subprocess.PIPE)
			s = subprocess.Popen(["./bin/func_{}".format(a), "10", "64", str(pol), "1"], stdin=seq.stdout, stdout=subprocess.PIPE)
		else:
			s = subprocess.Popen(["./bin/func_{}".format(a), "10", "64", str(pol), "1"], stdout=subprocess.PIPE)
		out = str(s.communicate()[0])
		out = out.split('\\n')
		s.stdout.close()
		for entry in out:
			if re.match(r"b'Hits", entry) or re.match(r"Hits",entry):
				total_hits += float(entry.split()[2])
			if re.match(r"Misses", entry):
				total_misses += float(entry.split()[2])

	avg_misses = total_misses / 4
	avg_hits = total_hits / 4
	
	print("Policy: {}, Avg Misses: {:.2f}, Avg Hits: {:.2f}".format(polsdict[pol], avg_misses, avg_hits))

print("======= END EVICTION POLICY TEST =======")

print()

print("======= THREADS TEST =======")

for i in range(5):
	for a in apps:
	
		if a == "collatz":
			seq = subprocess.Popen(["seq","1","100"],stdout=subprocess.PIPE)
			s = subprocess.Popen(["./bin/func_{}".format(a), "10", "64", "3", str(i+1)], stdin=seq.stdout, stdout=subprocess.PIPE)
		else:
			s = subprocess.Popen(["./bin/func_{}".format(a), "10", "64", "3", str(i+1)], stdout=subprocess.PIPE)
	
		out = str(s.communicate()[0])
		out = out.split('\\n')
		s.stdout.close()
		hits = 0
		misses = 0

		for entry in out:
			if re.match(r"b'Hits", entry) or re.match(r"Hits",entry):
				hits = int(entry.split()[2])
				total_hits += hits
			if re.match(r"Misses", entry):
				misses = int(entry.split()[2])
				total_misses += misses
		print(a)		
		print("Hits for {} thread(s): {}".format(i+1, total_hits))
		print("Misses for {} thread(s): {}".format(i+1, total_misses))
		print()
	
		total_misses = 0
		total_hits = 0
	

print("======= END THREADS TEST =======")

print()

print("======= GLOBAL VS LOCAL TEST =======")

for num in range(1,6):
	total_misses = 0
	total_hits = 0	
	total_local_misses = 0
	total_local_hits = 0
	print("NUMBER OF THREADS = {}".format(num))
	for pol in policies:
		seq = subprocess.Popen(["seq","1","100"],stdout=subprocess.PIPE)
		s = subprocess.Popen(["./bin/func_{}".format("collatz"), "10", "64", str(pol), str(num)], stdin=seq.stdout, stdout=subprocess.PIPE)
		out = str(s.communicate()[0])
		out = out.split('\\n')
		s.stdout.close()
		for entry in out:
			if re.match(r"b'Hits", entry) or re.match(r"Hits",entry):
				total_hits += float(entry.split()[2])
			if re.match(r"Misses", entry):
				total_misses += float(entry.split()[2])

		seq = subprocess.Popen(["seq","1","100"],stdout=subprocess.PIPE)
		s = subprocess.Popen(["./bin/func_{}".format("collatz_local"), "10", "64", str(pol), str(num)], stdin=seq.stdout, stdout=subprocess.PIPE)
		out = str(s.communicate()[0])
		out = out.split('\\n')
		s.stdout.close()
		for entry in out:
			if re.match(r"b'Hits", entry) or re.match(r"Hits",entry):
				total_local_hits += float(entry.split()[2])
			if re.match(r"Misses", entry):
				total_local_misses += float(entry.split()[2])

	avg_misses = total_misses / 4
	avg_hits = total_hits / 4
	avg_local_misses = total_local_misses / 4
	avg_local_hits = total_local_hits / 4 

	print("Avg Global Misses: {:.2f}, Avg Global Hits: {:.2f} Avg Local Misses: {:.2f} Avg Local Hits {:.2f}".format(avg_misses, avg_hits,avg_local_misses,avg_local_hits))


print("======= END GLOBAL VS LOCAL TEST =======")






