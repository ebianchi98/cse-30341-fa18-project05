/* func_prime.c: compute total of primes */

#include "memhashed/cache.h"
#include "memhashed/thread.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/* Constants */

#define N    (1<<10)

/* Thread */

void *	prime_thread(void *arg) {
    Cache *cache = (Cache *)(arg);
    size_t total;

    total = 0;
    for (uint64_t key = 0; key < N; key++) {
    	total += cache_get(cache, key);
    }
    assert(total == 172);
    
	total = 0;
    for (uint64_t key = 0; key < N; key++) {
    	total += cache_get(cache, key);
    }
    assert(total == 172);

    total = 0;
    for (uint64_t key = 0; key < 2*N; key++) {
    	total += cache_get(cache, key);
    }
    assert(total == 309);

    return NULL;
}

/* Handler */

int64_t	prime_handler(const uint64_t key) {
    if (key <= 1) {
    	return false;
    }

    for (uint64_t i = 2; i <= sqrt(key); i++) {
    	if ((key % i) == 0) {
    	    return false;
	}
    }

    return true;
}

/* Main execution */

int main(int argc, char *argv[]) {
    if (argc != 5) {
    	fprintf(stderr, "Usage: %s AddressLength PageSize EvictionPolicy Threads\n", argv[0]);
    	return EXIT_FAILURE;
    }

    size_t addrlen   = strtol(argv[1], NULL, 10);
    size_t page_size = strtol(argv[2], NULL, 10);
    Policy policy    = strtol(argv[3], NULL, 10);
    size_t nthreads  = strtol(argv[4], NULL, 10);

    Cache *cache = cache_create(addrlen, page_size, policy, prime_handler);
    assert(cache);

    Thread threads[nthreads];
    for (size_t t = 0; t < nthreads; t++) {
    	thread_create(&threads[t], NULL, prime_thread, cache);
    }

    for (size_t t = 0; t < nthreads; t++) {
    	thread_join(threads[t], NULL);
    }

    cache_stats(cache, stdout);
    cache_delete(cache);

    return EXIT_SUCCESS;
}
