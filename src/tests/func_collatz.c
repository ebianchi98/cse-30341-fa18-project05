/* func_collatz.c: compute collatz */

#include "memhashed/cache.h"
#include "memhashed/queue.h"
#include "memhashed/thread.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/* Constants */

#define SENTINEL (-1)

/* Globals */

Cache  *Collatz = NULL;
Queue  *Numbers = NULL;

/* Threads */

/**
 * Continuously computes the collatz length for the data in the Numbers Queue
 * until the SENTINEL is encountered.
 */
void *	collatz_thread(void *arg) {
    // Implement worker thread
	int64_t key = 0;
	while((key = queue_pop(Numbers)) != 0){
		printf("Collatz(%lu) = %lu\n", key, cache_get(Collatz, key));
	}
    return NULL;
}

/* Handler */

/**
 * Recursively computes collatz length for specified key.
 * @param   key	    Number to computer length for.
 * @return  Length of collatz sequence for specified key.
 */
int64_t	collatz_handler(const uint64_t key) {
    // Implement handler
	//debug("%zu", key);

	if(key == 1){
		return 1;
	}
	else{
		if(key % 2 == 1){
			// If it's odd, multiply by 3 and then add 1
			return cache_get(Collatz, (key*3)+1) + 1;
		}
		else{
			// If it's even, divide by 2
			return cache_get(Collatz, key/2) + 1;
		}
	}
}

/* Main execution */

int main(int argc, char *argv[]) {
    if (argc != 5) {
    	fprintf(stderr, "Usage: %s AddressLength PageSize EvictionPolicy Threads\n", argv[0]);
    	return EXIT_FAILURE;
    }

    // Parse command line arguments
    size_t addrlen   = strtol(argv[1], NULL, 10);
    size_t page_size = strtol(argv[2], NULL, 10);
    Policy policy    = strtol(argv[3], NULL, 10);
    size_t nthreads  = strtol(argv[4], NULL, 10);
	
    // Create Collatz Cache
    Collatz = cache_create(addrlen, page_size, policy, collatz_handler);
    assert(Collatz);
 
	// Create Numbers Queue
	Numbers = queue_create(0, 100);
	assert(Numbers);

	// Create Worker threads
    Thread threads[nthreads];
    for (size_t t = 0; t < nthreads; t++) {
    	thread_create(&threads[t], NULL, collatz_thread, (void *)t);
    }

    // Read numbers from standard input and add to Numbers queue
	int x;
	while (scanf("%d", &x) == 1) {
		queue_push(Numbers, x);
	}

	// Add sentinel value
	queue_push(Numbers, 0);

    // Join Worker threads
    for (size_t t = 0; t < nthreads; t++) {
    	thread_join(threads[t], NULL);
    }

    // Output Collatz Cache statistics
    cache_stats(Collatz, stdout);
    
    // Delete Collatz Cache and Numbers Queue
    cache_delete(Collatz);
	queue_delete(Numbers);
	
	return EXIT_SUCCESS;
}
