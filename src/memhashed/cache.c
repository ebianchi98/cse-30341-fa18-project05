/* cache.c: MemHashed Cache structure */

#include "memhashed/cache.h"

/* Cache Functions */

/**
 * Create Cache Structure.
 * @param   addrlen     Length of virtual address.
 * @param   page_size   Number of entries per page.
 * @param   policy      Which eviction policy to use.
 * @param   handler     Handler function to call on cache misses.
 * @return  Newly allocated Cache Structure with allocated pages.
 */
Cache *	cache_create(size_t addrlen, size_t page_size, Policy policy, Handler handler) {
    // TODO: Allocate Cache Structure and configure internal fields.
    Cache *c = calloc(1,sizeof(Cache));
    if(!c){
        return NULL;
    }
    c->addrlen = addrlen;
    c->page_size = page_size;
    c->policy = policy;
    c->handler = handler;
    if(addrlen == 0){
        c->naddresses = 0;
        c->npages = 0;
        c->vpn_shift = 0;
        c->vpn_mask = 0;
        c->offset_mask = 0;
        c->pages = NULL;
    }else{
        c->naddresses = 1 << addrlen;
        c->npages = (c->naddresses)/page_size;
        size_t shift_num = 0;
        size_t  start = c->npages;
        while(start != c->naddresses){
            start = start<<1;
            shift_num++;
        } 
        c->vpn_shift = shift_num;
        size_t mask_offset = addrlen - shift_num;
        c->vpn_mask = ((1<<mask_offset) - 1)<<shift_num;
        c->offset_mask = (1<<shift_num) - 1;
        c->pages = calloc(c->npages,sizeof(Page*));
        if(!c->pages){
             return NULL;
        }
        for(size_t i = 0; i < c->npages; i++){
            c->pages[i] = page_create(page_size,policy); 
        }
    }
    c->hits = 0;
    c->misses = 0;
    mutex_init(&c->lock,NULL); 
    return c;
}

/**
 * Delete Cache Structure (including internal pages).
 * @param   cache       Pointer to Cache Structure.
 */
void    cache_delete(Cache *cache) {
    // TODO: Deallocate Cache Structure and internal fields.
    for(size_t i = 0; i < cache->npages; i++){
        page_delete(cache->pages[i]);
    }
    free(cache->pages);
    free(cache);
}

/**
 * Retrieves value for specified for key:
 *
 *  1. Generate the virtual address.
 *
 *  2. Compute VPN and offset of virtual ddress using cache masks and shifts.
 *
 *  3. Use VPN and offset to lookup appropriate page:
 *
 *      a. If entry is found, then return value.
 *
 *      b. Otherwise, use the handler to generate the value for the
 *      corresponding key.
 *
 * @param   cache       Pointer to Cache Structure.
 * @param   key         Key used to identify corresponding Entry.
 * @return  value for the correspending key.
 */
int64_t	cache_get(Cache *cache, const uint64_t key) {
    
    mutex_lock(&cache->lock);
    size_t virtual_addr = key % cache->naddresses; 
    size_t vpn = (virtual_addr & cache->vpn_mask)>>cache->vpn_shift;
    size_t offset = virtual_addr & cache->offset_mask;
    
    Entry entry = page_get(cache->pages[vpn], key, offset); 
    
    
    if(entry.key != key){
        cache->misses++;
        mutex_unlock(&cache->lock);
        int64_t value = cache->handler(key);
        cache_put(cache, key, value);
        return value;
    }else{    
        cache->hits++;
        mutex_unlock(&cache->lock);
        return entry.value;
    }
}

/**
 * Insert or update value for specified key:
 *
 *  1. Generate the virtual address.
 *
 *  2. Compute VPN and offset of virtual ddress using cache masks and shifts.
 *
 *  3. Use VPN and offset to insert or update value into appropriate page.
 *
 * @param   cache       Pointer to Cache Structure.
 * @param   key         Key used to identify corresponding Entry.
 * @param   value       Value to update or replace old value with.
 */
void	cache_put(Cache *cache, const uint64_t key, const int64_t value) {
    
    mutex_lock(&cache->lock);
    
    size_t virtual_addr = key % cache->naddresses; 
    size_t vpn = (virtual_addr & cache->vpn_mask)>>cache->vpn_shift;
    size_t offset = virtual_addr & cache->offset_mask;
    
    page_put(cache->pages[vpn],key, value, offset);
    
    mutex_unlock(&cache->lock);

}

/**
 * Display cache hits and misses to specified streams.
 * @param   cache       Pointer to Cache Structure.
 * @param   stream      File stream to write to.
 */
void	cache_stats(Cache *cache, FILE *stream) {
    mutex_lock(&cache->lock);
    fprintf(stream, "Hits   = %lu\n", cache->hits);
    fprintf(stream, "Misses = %lu\n", cache->misses);
    mutex_unlock(&cache->lock);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
