/* page.c: MemHashed Page structure */

#include "memhashed/page.h"

/* Prototypes */

size_t  page_evict_fifo(Page *page, size_t offset);
size_t  page_evict_random(Page *page, size_t offset);
size_t  page_evict_lru(Page *page, size_t offset);
size_t  page_evict_clock(Page *page, size_t offset);

/* Functions */

void print_entries(Page *page) { 
    for(size_t i = 0; i < page->nentries; i++) {
        printf("page->entries[i].key: %zu\n",page->entries[i].key);
        printf("page->entries[i].value: %zu\n",page->entries[i].value);
        printf("page->entries[i].valid: %d\n",page->entries[i].valid);
        printf("page->entries[i].order: %zd\n",page->entries[i].order);
        printf("page->entries[i].used: %zd\n",page->entries[i].used);
    }
}

/**
 * Create Page Structure.
 * @param   nentries    Number of entries in page.
 * @param   policy      Which eviction policy to use.
 * @return  Newly allocated Page Structure with allocated entries.
 */
Page *	page_create(size_t nentries, Policy policy) {
    Page *page = calloc(1, sizeof(Page));
    if(!page) {
        return NULL;
    }
    page->nentries = nentries;
    page->policy = policy;
    page->order = 0;
    page->used = 0;
    page->entries = calloc(nentries, sizeof(Entry));
    mutex_init(&page->lock, NULL);
    return page;
}

/**
 * Delete Page Structure (including internal entries).
 * @param   page        Pointer to Page Structure.
 */
void    page_delete(Page *page) {
    if(page->entries) {
        free(page->entries);
    }
    free(page);
}

/**
 * Searches Page for Entry with specified key, beginning at specified offset.
 *
 *  This function performs a linear probe of the entries array beginning with
 *  the specified offset until a matching key is found or all entries have been
 *  probed.
 *
 * @param   page        Pointer to Page Structure.
 * @param   key         Key used to identify corresponding Entry.
 * @param   offset      Initial offset to begin probe.
 * @return  Entry with corresponding key or an Entry with a mismatched key if not found.
 */
Entry   page_get(Page *page, const uint64_t key, size_t offset) {
    Entry entry = {0};
    size_t of = 0;
    entry.key = ~key;
    if(offset >= page->nentries) {
        return entry;
    }
    if(!page) return entry;
    mutex_lock(&page->lock);
    of = offset;
    for(size_t i = 0; i < page->nentries; i++) {
        if(page->entries[of].valid) {
            if(key == page->entries[of].key) {
                page->entries[of].used = ++page->used;
                mutex_unlock(&page->lock);
                return page->entries[of];
            }
        }
        of++;
        if(of == page->nentries) of = 0;
    }
    mutex_unlock(&page->lock);
    return entry;
}

/**
 * Insert or update Entry with specified key with the new value.
 *
 *  This function begins a linear probe for the Entry with the corresponding
 *  key:
 *
 *      1. If it is found, then the Entry is updated.
 *
 *      2. If it is not found, then the first invalid Entry is used to insert a
 *      new key and value.
 *
 *      3. If it is not found and there are no invalid Entries, then one is
 *      evicted based on the previously specified Policy and then used
 *      to insert a new key and value.
 *
 * @param   page        Pointer to Page Structure.
 * @param   key         Key used to identify corresponding Entry.
 * @parm    value       Value to update or replace old value with.
 * @param   offset      Initial offset to begin probe.
 */
void    page_put(Page *page, const uint64_t key, const int64_t value, size_t offset) {
    mutex_lock(&page->lock);
    size_t of = 0;
    of = offset;
    for(size_t i = 0; i < page->nentries; i++) {
        // key matches
        if(page->entries[of].valid) {
            if(key == page->entries[of].key) {
                page->entries[of].value = value;
                page->entries[of].used = ++page->used;
                page->entries[of].valid = 1;
                mutex_unlock(&page->lock);
                return;
            }
        }
        of++;
        if(of == page->nentries) of = 0;
    }
    of = offset;
    for(size_t k = 0; k < page->nentries; k++) {
        // invalid entry
        if(!page->entries[of].valid) {
            page->entries[of].key = key;
            page->entries[of].value = value;
            page->entries[of].order = ++page->order;
            page->entries[of].used = ++page->used;
            page->entries[of].valid = 1;
            mutex_unlock(&page->lock);
            return;
        }
        of++;
        if(of == page->nentries) of = 0;
    }
    // need to evict
    size_t entry = 0;
    switch(page->policy) {
        case FIFO:
            entry = page_evict_fifo(page, offset);
            break;
        case RANDOM:
            entry = page_evict_random(page, offset);
            break;
        case LRU:
            entry = page_evict_lru(page, offset);
            break;
        case CLOCK:
            entry = page_evict_clock(page, offset);
            break;
    } 
    page->entries[entry].key = key;
    page->entries[entry].value = value;
    page->entries[entry].order = ++page->order;
    page->entries[entry].used = ++page->used;

    mutex_unlock(&page->lock);
}

/**
 * Select Entry to evict based on FIFO strategy.
 * @param   page        Pointer to Page Structure.
 * @param   offset      Initial offset to begin probe.
 * @return  Offset of Entry to evict.
 */
size_t  page_evict_fifo(Page *page, size_t offset) {
    for(size_t i = 0; i < page->nentries; i++) {
        if(page->entries[i].order < page->entries[offset].order) {
            offset = i;
        }
    }
    return offset;
}

/**
 * Select Entry to evict based on Random strategy.
 * @param   page        Pointer to Page Structure.
 * @param   offset      Initial offset to begin probe.
 * @return  Offset of Entry to evict.
 */
size_t  page_evict_random(Page *page, size_t offset) {
    offset = (size_t) rand() % page->nentries;
    return offset;
}

/**
 * Select Entry to evict based on LRU strategy.
 * @param   page        Pointer to Page Structure.
 * @param   offset      Initial offset to begin probe.
 * @return  Offset of Entry to evict.
 */
size_t  page_evict_lru(Page *page, size_t offset) {
    for(size_t i = 0; i < page->nentries; i++) {
        if(page->entries[i].used < page->entries[offset].used) {
            offset = i;
        }
    }
    return offset;
}

/**
 * Select Entry to evict based on Clock strategy.
 * @param   page        Pointer to Page Structure.
 * @param   offset      Initial offset to begin probe.
 * @return  Offset of Entry to evict.
 */
size_t  page_evict_clock(Page *page, size_t offset) {
    bool evicted = false;
    while(!evicted) {
        if(page->entries[offset].used == 0) {
            evicted = true;
            break;
        } else {
            page->entries[offset].used = 0;
        }
        offset++;
        if(offset == page->nentries) offset = 0;
    }
    return offset;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
