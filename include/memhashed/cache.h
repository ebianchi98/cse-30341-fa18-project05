/* cache.h: MemHashed Cache Structure */

#ifndef CACHE_H
#define CACHE_H

#include "memhashed/page.h"
#include "memhashed/thread.h"
#include "memhashed/types.h"

#include <stdio.h>

/* Cache Structure */

typedef struct {
    size_t      addrlen;	    /* Length of virtual address */
    size_t      page_size;	    /* Number of entries per page */
    Policy      policy;	            /* Evicition policy */
    Handler     handler;	    /* Handler for misses */

    size_t      naddresses;	    /* Number of addresses */
    size_t	npages;	            /* Number of pages */
    size_t      vpn_shift;	    /* VPN Bit Shift */
    size_t      vpn_mask;	    /* VPN Bit Mask */
    size_t      offset_mask;        /* Offset Bit Mask */

    size_t      hits;	            /* Number of cache hits */
    size_t	misses;	            /* Number of cache misses */

    Page      **pages;	            /* Page table */

    Mutex       lock;               /* Lock */
} Cache;

/* Cache Functions */

Cache *	    cache_create(size_t addrlen, size_t page_size, Policy policy, Handler handler);
void	    cache_delete(Cache *cache);

int64_t	    cache_get(Cache *cache, const uint64_t key);
void	    cache_put(Cache *cache, const uint64_t key, const int64_t value);

void	    cache_stats(Cache *cache, FILE *stream);

#endif

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
