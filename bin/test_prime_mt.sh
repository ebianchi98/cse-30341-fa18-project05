#!/bin/bash

run-test() {
    name=$1
    policy=$2
    echo -n "Testing   prime (mt) with $name ... "
    if ./bin/func_prime 10 64 $policy 8 > /dev/null; then
	echo Success
    else
	echo Failure
    fi
}

SCRATCH=$(mktemp -d)
trap "rm -fr $SCRATCH" INT QUIT TERM EXIT

run-test FIFO   0
run-test RANDOM 1
run-test LRU    2
run-test CLOCK  3
